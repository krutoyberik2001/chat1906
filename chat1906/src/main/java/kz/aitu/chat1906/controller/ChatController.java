package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.repository.ChatRepository;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chat")
public class ChatController {
    private final ChatRepository chatRepository;
private final ChatService chatService;
    private  final AuthService authService;
    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(chatRepository.findAll());
    }
    @PostMapping
    public ResponseEntity<?> addChat(@RequestBody Chat chat,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid token");
        }
        chatService.add(chat);
        return ResponseEntity.ok("Chat successfully added");
    }

    @PutMapping
    public ResponseEntity<?> editChat(@RequestBody Chat chat,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid token");
        }
        chatService.update(chat);
        return ResponseEntity.ok(chat);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteChat(@RequestBody Chat chat,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid token");
        }
        chatService.delete(chat);
        return ResponseEntity.ok("Chat successfully deleted");
    }
}
