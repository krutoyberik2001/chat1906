package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.ParticipantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor

    @RequestMapping("/api/v1/participant")
    public class ParticipantController {

        private final ParticipantService participantService;
    private  final AuthService authService;
    @PostMapping
    public ResponseEntity<?> addParticipant(@RequestBody Participant participant,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }

        participantService.add(participant);
        return ResponseEntity.ok("Participant successfully added");
    }

    @DeleteMapping
    public ResponseEntity<?> deleteParticipant(@RequestBody Participant participant,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Token is not valid!");
        }
        participantService.delete(participant);
        return ResponseEntity.ok("Participant successfully deleted");
    }
        @GetMapping({"/chat/{id}"})
        public ResponseEntity<?> getParticipantByChatId(@PathVariable(name = "id") Long chatId,@RequestHeader String token){
            if(!authService.getToken(token).equals("exists")) {
                return ResponseEntity.ok("Token is not valid!");
            }
        return ResponseEntity.ok(participantService.getUsersByChat(chatId));
        }

        @GetMapping({"/user/{id}"})
        public ResponseEntity<?> getParticipantByUserId(@PathVariable(name = "id") Long userId,@RequestHeader String token){
            if(!authService.getToken(token).equals("exists")) {
                return ResponseEntity.ok("Token is not valid!");
            }
        return ResponseEntity.ok(participantService.getParticipantByUser(userId));
        }

    }



