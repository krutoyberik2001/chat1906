package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.repository.MessageRepository;
import kz.aitu.chat1906.repository.ParticipantRepository;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.MessageService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService messageService;
    private final  MessageRepository messageRepository;
    private final ParticipantRepository participantRepository;
    private  final AuthService authService;
    @GetMapping({"/chat/{id}"})
    public ResponseEntity<?> getMessagesByChatId(@PathVariable(name = "id") Long chatId,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid token");
        }
        return ResponseEntity.ok(messageService.getAllChatById(chatId));
    }

    @GetMapping("/getTenMessagesByChatId/{id}")
    public ResponseEntity<?> getTenMessageByChatId(@PathVariable Long id, @RequestHeader  Long userId) {
        if (!participantRepository.existsByChatIdAndUserId(id, userId)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<Message> messageList = messageService.findByChatIdAndUserIdNot(id, userId);
        Date date = new Date();

        for (Message message : messageList) {

            if (!message.getDelivered() ) {
                message.setDelivered(true);
                message.setDeliveredTimestamp(date.getTime());
                messageService.update(message);

            }


        }
        return ResponseEntity.ok(messageService.getTenMessagesByChatId(id));
    }
    @GetMapping("/read/{id}")
    public ResponseEntity<?> readMessage(@PathVariable Long id, @RequestHeader Long userId) {
        if (!participantRepository.existsByChatIdAndUserId(id, userId)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

            List<Message> messageList = messageService.findByChatIdAndUserIdNot(id, userId);
            Date date = new Date();

            for (Message message : messageList) {
                if (message.getRead() == false) {
                    message.setRead(true);
                    message.setReadTimestamp(date.getTime());
                    messageService.update(message);

                }
        }
        return ResponseEntity.ok("The message read");
    }
    @PostMapping("/status/{chatId}")
    public ResponseEntity<?> statusMessage(@RequestHeader("status") String status,
                                           @RequestHeader("userId") Long userId,
                                           @PathVariable Long chatId,@RequestHeader String token){
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid token");
        }

        if(status.equals("online") || status.equals("writing")){
            Date date=new Date();
            Message message = new Message();
            message.setUserId(userId);
            message.setCreatedTimestamp(date.getTime());
            message.setDelivered(false);
            message.setRead(false);
            message.setUpdatedTimestamp(date.getTime());
            message.setChatId(chatId);


            message.setMessageType(status);
            messageRepository.save(message);
            return ResponseEntity.ok(message);


        }

        else{
            return  new ResponseEntity<>(HttpStatus.FORBIDDEN);

        }


    }
    @GetMapping("/getAllNotDelivered/{chatId}")
    public ResponseEntity<?> getNotDelivered(@RequestHeader Long userId,@PathVariable Long chatId,@RequestHeader String token){
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid Token");
        }
        if(participantRepository.existsByChatIdAndUserId(chatId, userId)){
            return ResponseEntity.ok(messageService.getNotDelivered(chatId,userId));
        }
        else{
           return ResponseEntity.ok("This user doesnt belong to this chat!!!");
        }

    }



    @GetMapping("/getTenMessagesByUserId/{id}")
    public ResponseEntity<?> getTenMessageByUserId(@PathVariable Long id,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid Token");
        }
        return ResponseEntity.ok(messageService.getTenMessagesByUserId(id));
    }

    @PostMapping("/create")
    public ResponseEntity<?> createMessage(@RequestBody Message message,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid Token");
        }
        Date date=new Date();
        message.setCreatedTimestamp(date.getTime());
        message.setUpdatedTimestamp(date.getTime());
        message.setDelivered(false);
        message.setRead(false);
        message.setMessageType("basic");
        messageService.add(message);
        return ResponseEntity.ok("The message successfully added");
    }
    @PutMapping
    public ResponseEntity<?> editMessage(@RequestBody Message message,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid Token");
        }
        messageService.update(message);
        return ResponseEntity.ok(message);
    }
    @DeleteMapping
    public ResponseEntity<?> deleteMessage(@RequestBody Message message,@RequestHeader String token) {
        if(!authService.getToken(token).equals("exists")) {
            return ResponseEntity.ok("Invalid Token");
        }
        messageService.delete(message);
        return ResponseEntity.ok("The message successfully deleted");
    }


}

