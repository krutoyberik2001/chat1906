package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChatService {
    @Autowired
    private   ChatRepository chatRepository;
    public void add(Chat chat) {
        chatRepository.save(chat);
    }

    public void update(Chat chat) {
chatRepository.save(chat);
    }

    public void delete(Chat chat) {
        chatRepository.delete(chat);
    }
}
